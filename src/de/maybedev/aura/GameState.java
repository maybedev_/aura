package de.maybedev.aura;

public enum GameState {
    Lobby, Ingame, End;

    private static GameState currentstate;


    public static void setState(GameState state) {
        currentstate = state;
    }

    public static GameState getState(GameState state) {
        return currentstate;
    }

    public static void setCurrentstate(GameState currentstate) {
        GameState.currentstate = currentstate;
    }

    public static GameState getCurrentstate() {
        return currentstate;
    }
}

