package de.maybedev.aura.listener;

import de.maybecode.coderapi.util.ItemBuilder;
import de.maybedev.aura.Main;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;

public class PlayerTeleporter implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent e){

        Player p = e.getPlayer();

        if(e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
            if(p.getItemInHand() == null) return;
            if(p.getItemInHand().getItemMeta() == null) return;
            if(p.getItemInHand().getType() == null) return;
            if(p.getItemInHand().getItemMeta().getDisplayName() == null) return;
            if(p.getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase("§7•● §bTeleporter §8» §7§oRechtsklick")){
                Inventory inv = Bukkit.createInventory(null, 3*9, "§e§lTeleporter");

                for(Player all : Main.players) {
                    inv.addItem(new ItemBuilder(Material.SKULL_ITEM, 1, (short) 3).setName("§7" + all.getName()).setSkullOwner(all.getName()).form());
                }
                for (int i = 0; i < inv.getSize(); i++) {
                    if (inv.getItem(i) == null) {
                        inv.setItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 7).setName(" ").form());
                    }
                }
                p.openInventory(inv);
            }
        }

    }

    @EventHandler
    public void onClick(InventoryClickEvent e){
        Player p = (Player) e.getWhoClicked();
        if(e.getInventory().getName().equalsIgnoreCase("§e§lTeleporter")) {
            if(e.getCurrentItem() == null) return;
            if(e.getCurrentItem().getType() == null) return;
            if(e.getCurrentItem().getItemMeta() == null) return;
            if(e.getCurrentItem().getItemMeta().getDisplayName() == null) return;
            e.setCancelled(true);
            if (e.getCurrentItem().getType() == Material.SKULL_ITEM) {
                String name = e.getCurrentItem().getItemMeta().getDisplayName();
                name = name.replace("§7", "");
                if(Bukkit.getPlayer(name) != null) {
                    if(Main.players.contains(Bukkit.getPlayer(name))) {
                        p.teleport(Bukkit.getPlayer(name));
                        p.closeInventory();
                        p.playSound(p.getLocation(), Sound.ENDERMAN_TELEPORT, 1.0f, 1.0f);
                    } else {
                        p.closeInventory();
                        p.sendMessage(Main.PREFIX + "§cDer Spieler ist bereits ausgeschieden!");
                    }
                } else {
                    p.closeInventory();
                    p.sendMessage(Main.PREFIX + "§cDer Spieler ist offline!");
                }
            }
        }
    }
}
