package de.maybedev.aura.listener;

import de.maybecode.coderapi.util.ItemBuilder;
import de.maybedev.aura.GameState;
import de.maybedev.aura.Main;
import de.maybedev.aura.manager.GameManager;
import de.maybedev.aura.manager.PlayerManager;
import de.maybedev.aura.util.Board;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.player.PlayerAchievementAwardedEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.material.MaterialData;

public class DeactivateListener implements Listener {

    /*
    Weather
    Achievments
    Drop
    Pickup
    Place, Break (ONLY TNT)
     */

    @EventHandler
    public void onAchievment(PlayerAchievementAwardedEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onPlace(BlockPlaceEvent e) {
        if(e.getBlock().getType() == Material.TNT) {
            e.setCancelled(false);
        } else {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onBreak(BlockBreakEvent e) {
        if (e.getBlock().getType() == Material.TNT) {
            e.setCancelled(true);
            e.getBlock().setType(Material.AIR);
            e.getPlayer().getInventory().addItem(new ItemBuilder(Material.TNT).setName("§c§lTNT").form());
        } else {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onExplode(BlockExplodeEvent e) {
        e.setYield(0.1f);
    }

    @EventHandler
    public void onSpawn(EntitySpawnEvent e) {
        if(e.getEntity() instanceof Item) {
            e.setCancelled(true);
        }
    }


    @EventHandler
    public void onPickup(PlayerPickupItemEvent e){
        e.setCancelled(true);
    }



    @EventHandler
    public void onWeather(WeatherChangeEvent e){
        e.setCancelled(true);
    }

    @EventHandler
    public void onDamage(EntityDamageEvent e){

        Player p = (Player) e.getEntity();
        if(GameState.getCurrentstate() == GameState.Lobby){
            e.setCancelled(true);
        } else if (Main.protection) {
                e.setCancelled(true);
            } else  if(GameState.getCurrentstate() == GameState.End){
                e.setCancelled(true);
            } else {
            e.setDamage(e.getDamage() / 2);
        }



    }

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent e) {
        if(e.getDamager() instanceof Player) {
            Player damager = (Player) e.getDamager();
            if(!Main.players.contains(damager)) {
                e.setCancelled(true);
            } else {
                e.setCancelled(false);
            }
        }
    }



    @EventHandler
    public void onFood(FoodLevelChangeEvent e){
        e.setCancelled(true);
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent e){
        Player p = e.getEntity();
        e.setDeathMessage(null);
        Bukkit.broadcastMessage(Main.PREFIX + "Der Spieler §a" + p.getName() + " §7ist ausgeschieden");
        p.spigot().respawn();
            Main.players.remove(e.getEntity());
            e.getEntity().setAllowFlight(true);
            e.getEntity().setFlying(true);
            for (Player all : Bukkit.getOnlinePlayers()) {
                Board.setBoard(all);
                if (Main.players.contains(all)) {
                    all.hidePlayer(e.getEntity());
                } else {
                    e.getEntity().showPlayer(all);
                }
            }
            e.getEntity().sendMessage(Main.PREFIX + "Du bist nun §aSpectator!");
            if (Main.players.size() <= 1) {
                GameManager.stopGame();
            }


    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent e){
        Player p = e.getPlayer();
        PlayerManager.setSpecItem(p);
    }

}
