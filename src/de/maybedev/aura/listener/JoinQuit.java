package de.maybedev.aura.listener;

import de.maybedev.aura.GameState;
import de.maybedev.aura.Main;
import de.maybedev.aura.manager.GameManager;
import de.maybedev.aura.manager.PlayerManager;
import de.maybedev.aura.manager.Spammer;
import de.maybedev.aura.manager.WarpManager;
import de.maybedev.aura.util.Board;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;

public class JoinQuit implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {

        Player p = e.getPlayer();
        e.setJoinMessage(null);
        p.setMaxHealth(20);


        if (GameState.getCurrentstate() == GameState.Lobby) {

            Bukkit.broadcastMessage(Main.PREFIX + p.getName() + " §7hat die Runde §abetreten");
            readyPlayer(p);
            p.teleport(WarpManager.getWarp("Lobby"));
            if (Bukkit.getOnlinePlayers().size() >= 2) {
                Spammer.startCount();
            } else {
                Main.sendNedded();
            }
        } else {
            for (Player all : Bukkit.getOnlinePlayers()) {
                if (Main.players.contains(all)) {
                    all.hidePlayer(p);
                } else {
                    p.showPlayer(all);
                }
            }
            p.sendMessage(Main.PREFIX + "Du bist nun §aSpectator");
            p.getInventory().clear();
            p.getInventory().setArmorContents(null);
            PlayerManager.setSpecItem(p);
            p.setAllowFlight(true);
            p.setFlying(true);
        }
        Board.setBoard(e.getPlayer());
    }

    private void readyPlayer(Player p) {
        p.setHealth(p.getMaxHealth());
        p.setFoodLevel(20);
        p.getInventory().clear();
        p.getInventory().setArmorContents(null);
        p.setGameMode(GameMode.ADVENTURE);
        p.setFireTicks(0);
        p.setFlying(false);
        p.setAllowFlight(false);
        for (PotionEffect potionEffect : p.getActivePotionEffects()) {
            p.removePotionEffect(potionEffect.getType());
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        e.setQuitMessage(null);
        if (GameState.getCurrentstate() == GameState.Lobby) {
            Bukkit.broadcastMessage(Main.PREFIX + "§7" + p.getName() + "§7 hat das Spiel §cverlassen!");
            Bukkit.getScheduler().runTaskLater(Main.getPlugin(Main.class), new BukkitRunnable() {
                @Override
                public void run() {
                    Main.sendNedded();
                }
            }, 20L);
            // Lobby verlass - Nachricht
        } else if (GameState.getCurrentstate() == GameState.Ingame) {
            if (PlayerManager.isPlayer(p)) {
                Bukkit.broadcastMessage(Main.PREFIX + "§7" + p.getName() + "§7 hat das Spiel §cverlassen!");
                PlayerManager.removePlayer(p);
                if (Main.players.size() <= 1) {
                    Bukkit.broadcastMessage(Main.PREFIX + "§cDas Spiel ist vorbei!");
                    GameManager.stopGame();
                }
            }
        }
        Board.setBoard(e.getPlayer());
    }
}