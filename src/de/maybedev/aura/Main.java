package de.maybedev.aura;

import de.maybecode.coderapi.manager.Methods;
import de.maybedev.aura.commands.StartCMD;
import de.maybedev.aura.commands.setWarp;
import de.maybedev.aura.listener.DeactivateListener;
import de.maybedev.aura.listener.JoinQuit;
import de.maybedev.aura.listener.PlayerTeleporter;
import de.maybedev.aura.manager.ChestManager;
import de.maybedev.aura.manager.Spammer;
import org.bukkit.Bukkit;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Scoreboard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Main extends JavaPlugin {


    public static Main instance;
    public static String PREFIX = "§aAura §8● §7";
    public static String NO_PERMISSION = "§aAura §8● §cDu hast keine Rechte";
    public static int MIN_PLAYER = 2;
    public static ArrayList<Player> players = new ArrayList<>();
    public static boolean protection = false;

    public static Main getInstance()  {
        return instance;
    }

    @Override
    public void onEnable() {
        getServer().getPluginManager().getPlugins();

        instance = this;
        Bukkit.getConsoleSender().sendMessage(Main.PREFIX + "§7Das Plugin wurde §a§lAktiviert!");
        ChestManager.removeChest();

        PluginManager pm = Bukkit.getPluginManager();

        pm.registerEvents(new JoinQuit(), this);
        pm.registerEvents(new DeactivateListener(), this);
        pm.registerEvents(new PlayerTeleporter(), this);


        getCommand("setwarp").setExecutor(new setWarp());
        getCommand("start").setExecutor(new StartCMD());
        GameState.setState(GameState.Lobby);

    }

    public static void sendNedded() {
        if (Bukkit.getOnlinePlayers().size() < MIN_PLAYER) {
            Spammer.stopCount();
            for(Player all : Bukkit.getOnlinePlayers()) {
                Methods.sendActionBar(all, "§cEs werden mindestens " + MIN_PLAYER + " Spieler benötigt!");
            }
            Bukkit.getScheduler().runTaskLater(Main.getPlugin(Main.class), new BukkitRunnable() {
                @Override
                public void run() {
                    sendNedded();
                }
            }, 40L);
        }
    }

    @Override
    public void onDisable() {
        Bukkit.getConsoleSender().sendMessage(Main.PREFIX + "§7Das Plugin wurde §c§lDeaktiviert!");
    }
}
