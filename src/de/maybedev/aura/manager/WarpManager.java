package de.maybedev.aura.manager;


import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class WarpManager {

    public static File file = new File("plugins/Aura//Config.yml");
    public static FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);

    public static void save() throws IOException {
        cfg.save(file);
    }



    public static Location getWarp(String warp) {
        warp = warp.toLowerCase();
        return (Location) cfg.get(warp);
    }

    public static void setWarp(String warp, Location loc) {
        warp = warp.toLowerCase();
        cfg.set(warp, loc);
        try {
            cfg.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean getChest(String warp) {
        warp = warp.toLowerCase();
        return (boolean) cfg.get(warp);
    }


}
