package de.maybedev.aura.manager;

import de.maybecode.coderapi.manager.Methods;
import de.maybedev.aura.Main;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class StopCountdown {

    public static int count_value = 3;
    public static int count_stop = 11;

    public static void startCount() {
        if (!isRunning()) {
            count_value = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getInstance(), new BukkitRunnable() {
                @Override
                public void run() {
                    --count_stop;
                    for (Player all : Bukkit.getOnlinePlayers()) {
                        Methods.sendActionBar(all, "§cDer Server stoppt in " + count_stop + " §cSekunde(n)");
                    }
                    if (count_stop == 0) {
                        stopCount();
                        Bukkit.shutdown();
                    }
                }
            }, 0L, 20L);
        }
    }

    public static boolean isRunning() {
        return count_value != 3;
    }

    public static void stopCount() {
        Bukkit.getScheduler().cancelTask(count_value);
        count_value = 3;
    }
}
