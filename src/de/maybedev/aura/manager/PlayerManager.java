package de.maybedev.aura.manager;

import de.maybecode.coderapi.util.ItemBuilder;
import de.maybedev.aura.Main;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class PlayerManager {

    public static void addPlayer(Player player) {
        Main.players.add(player);
    }

    public static void removePlayer(Player player) {
        Main.players.remove(player);
    }

    public static boolean isPlayer(Player player) {
        return Main.players.contains(player);
    }


    public static void setPlayerItems(Player player){
        player.getInventory().setItem(0, new ItemBuilder(Material.STICK).addEnchantment(Enchantment.KNOCKBACK, 3).setName("§9§lKnüppel").form());
        player.getInventory().setItem(1, new ItemBuilder(Material.FISHING_ROD).setName("§6§lRute").form());
        player.getInventory().setItem(2, new ItemBuilder(Material.ENDER_PEARL, 32).setName("§1§lEnderperle").form());
        player.getInventory().setItem(3, new ItemBuilder(Material.TNT, 16).setName("§c§lTNT").form());
        player.getInventory().setItem(4, new ItemBuilder(Material.FLINT_AND_STEEL).setName("§3§lFeuerzeug").form());
        player.getInventory().setItem(7, new ItemBuilder(Material.POTION, 1, (short) 8193).setName("§d§lRegeneration").form());
        player.getInventory().setItem(8, new ItemBuilder(Material.POTION, 1, (short) 8226).setName("§b§lGeschwindigkeit").form());

        player.getInventory().setHelmet(new ItemBuilder(Material.LEATHER_HELMET).setData(DyeColor.BLACK.getData()).form());
        player.getInventory().setChestplate(new ItemBuilder(Material.CHAINMAIL_CHESTPLATE).form());
        player.getInventory().setLeggings(new ItemBuilder(Material.LEATHER_LEGGINGS).setData(DyeColor.BLACK.getData()).form());
        player.getInventory().setBoots(new ItemBuilder(Material.CHAINMAIL_BOOTS).addEnchantment(Enchantment.PROTECTION_FALL, 8).form());


    }

    public static void setSpecItem(Player player){
        Bukkit.getScheduler().runTaskLater(Main.getInstance(), new BukkitRunnable() {
            @Override
            public void run() {
                player.getInventory().setItem(4, new ItemBuilder(Material.COMPASS).setName("§7•● §bTeleporter §8» §7§oRechtsklick").form());
            }
        }, 2L);
    }
}



