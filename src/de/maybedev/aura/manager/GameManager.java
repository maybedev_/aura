package de.maybedev.aura.manager;

import de.maybecode.coderapi.manager.Methods;
import de.maybedev.aura.GameState;
import de.maybedev.aura.Main;
import de.maybedev.aura.util.Board;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class GameManager {

    public static void startGame() {
        Main.protection = true;
        GameState.setState(GameState.Ingame);
        ChestManager.setChest();
        for (Player all : Bukkit.getOnlinePlayers()) {
            all.teleport(WarpManager.getWarp("Spawn"));
            all.playSound(all.getLocation(), Sound.LEVEL_UP, 1.0f, 1.0f);
            PlayerManager.addPlayer(all);

        }
        for(Player all : Bukkit.getOnlinePlayers()) {
            Board.setBoard(all);
            PlayerManager.setPlayerItems(all);
            Methods.sendTitle(all, 20, 60, 20, "§cDie Spawnprotection endet", "§cin §e10 §cSekunden");
        }
        Bukkit.getScheduler().runTaskLater(Main.getInstance(), new BukkitRunnable() {
            @Override
            public void run() {
                Main.protection = false;
                Bukkit.broadcastMessage(Main.PREFIX + "§7Die Spawnprotection wurde §cdeaktiviert!");
                for(Player all : Bukkit.getOnlinePlayers()) {
                    Methods.sendTitle(all, 20, 60, 20, "§7Die Spawnprotection wurde §cdeaktiviert!", "");
                }
            }
        }, 200L);
    }

    public static void stopGame() {
        GameState.setState(GameState.End);
        for (Player all : Bukkit.getOnlinePlayers()) {
            all.teleport(WarpManager.getWarp("Lobby"));
            Board.setBoard(all);
            all.getInventory().clear();
            all.getInventory().setArmorContents(null);
            all.setGameMode(GameMode.ADVENTURE);
            all.setFlying(false);
            all.setAllowFlight(false);

            for (Player all1 : Bukkit.getOnlinePlayers()){
                all1.showPlayer(all);
                all.showPlayer(all1);

            }

        }
        StopCountdown.startCount();
    }

}
