package de.maybedev.aura.manager;

import de.maybecode.coderapi.manager.Methods;
import de.maybedev.aura.Main;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class Spammer {

    public static int count_value = 2;
    public static int count_start = 61;

    public static void startCount() {
        if (!isRunning()) {
            count_value = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getInstance(), new BukkitRunnable() {
                @Override
                public void run() {
                    --count_start;
                    for (Player all : Bukkit.getOnlinePlayers()) {
                        Methods.sendActionBar(all, "§aDie Runde startet in " + count_start + " §aSekunde(n)");
                    }
                    if (count_start == 0) {
                        stopCount();
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.setGameMode(GameMode.SURVIVAL);
                        }
                        GameManager.startGame();
                    }
                }
            }, 0L, 20L);
        }
    }

    public static boolean isRunning() {
        return count_value != 2;
    }

    public static void stopCount() {
        Bukkit.getScheduler().cancelTask(count_value);
        count_value = 2;
        count_start = 61;
    }

}
