package de.maybedev.aura.util;

import de.maybedev.aura.GameState;
import de.maybedev.aura.Main;
import de.musterbukkit.replaysystem.main.ReplayAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

public class Board {

    public static String getReplayID() {
        if(Bukkit.getPluginManager().getPlugin("ReplaySystem") != null) {
            return "§6" + ReplayAPI.getReplayID();
        } else {
            return "§cDEAKTIVIERT";
        }
    }

    public static void setBoard(Player player) {
        Scoreboard scoreboard;
        if (player.getScoreboard() != null)
            scoreboard = player.getScoreboard();
        scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        Objective objective = scoreboard.registerNewObjective("Board", "dummy");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName("§8•● §aAura §8●•");
        objective.getScore(" ").setScore(6);
        if(GameState.getCurrentstate() == GameState.Lobby) {
            objective.getScore("§7§lK/D").setScore(5);
            objective.getScore("§7» §cDeaktiviert!").setScore(4);
            objective.getScore(" ").setScore(3);
            objective.getScore("§7§lReplayID").setScore(2);
            objective.getScore("§7» " + getReplayID()).setScore(1);
        } else if(GameState.getCurrentstate() == GameState.Ingame){
                objective.getScore("§7§lLebende Spieler").setScore(5);
                objective.getScore("§7» " + Main.players.size()).setScore(4);

            objective.getScore(" ").setScore(3);
            objective.getScore("§7§lReplayID").setScore(2);
            objective.getScore("§7» " + getReplayID()).setScore(1);
        } else if(GameState.getCurrentstate() == GameState.End) {
            objective.getScore("§7K/D").setScore(5);
            objective.getScore("§7» §cDeaktiviert!").setScore(4);
            objective.getScore(" ").setScore(3);
            objective.getScore("§7§lReplayID").setScore(2);
            objective.getScore("§7» " + getReplayID()).setScore(1);
        }
        player.setScoreboard(scoreboard);
    }
}
