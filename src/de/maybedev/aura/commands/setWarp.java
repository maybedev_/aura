package de.maybedev.aura.commands;

import de.maybecode.coderapi.CoderAPI;
import de.maybedev.aura.Main;
import de.maybedev.aura.manager.WarpManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class setWarp implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (!(sender instanceof Player)) {
            CoderAPI.sendConsoleMessage(Main.PREFIX + "§cDu bist kein Spieler");
            return false;
        }
        Player p = (Player) sender;
        if (command.getName().equalsIgnoreCase("setwarp")) {
            if (p.hasPermission("Aura.Admin")) {
                if (args.length == 0) {
                    p.sendMessage(Main.PREFIX + "§cBitte benutze /setwarp <Warpname>");
                } else if (args.length == 1) {
                    String warp = args[0];
                    WarpManager.setWarp(warp, p.getLocation());
                    p.sendMessage(Main.PREFIX + "§7Der Warp §a" + warp + " §7wurde gesetzt");
                } else {
                    p.sendMessage(Main.PREFIX + "§cBitte benutze /setwarp <Warpname>");
                }
            } else {
                p.sendMessage(Main.NO_PERMISSION);
            }
        }
        return false;
    }
}
