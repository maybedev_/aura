package de.maybedev.aura.commands;

import de.maybecode.coderapi.CoderAPI;
import de.maybecode.coderapi.interfaces.CoderCommand;
import de.maybedev.aura.Main;
import de.maybedev.aura.manager.Spammer;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class StartCMD extends CoderCommand {


    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if(!(sender instanceof Player)){
            CoderAPI.sendConsoleMessage("§cDu bist kein Spieler");
            return false;
        }

        Player p = (Player)sender;
        if(cmd.getName().equalsIgnoreCase("start")){
            if(p.hasPermission("Aura.SUP")){
                if(Spammer.count_start <= 10) {
                    p.sendMessage(Main.PREFIX + "§7Die §aRunde §7wurde bereits gestartet!");
                    return false;
                } else if(Bukkit.getOnlinePlayers().size() < 2){
                    p.sendMessage(Main.PREFIX + "§cEs werden mindestens 2 Spieler benötigt!");
                    return false;
                }
                Spammer.count_start = 11;
                Bukkit.broadcastMessage(Main.PREFIX + "Der Countdown wurde verkürzt!");
            } else {
                p.sendMessage(Main.NO_PERMISSION);
            }
        }

        return super.onCommand(sender, cmd, label, args);
    }
}
